/*
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <ros/ros.h>

#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseStampedVector.h"

#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <algorithm>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
//#include <strstream>

#include <vector>

#include <math.h>

using namespace visualization_msgs;


// %Tag(vars)%
boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server;
int counter = 1;
int counter_2 = 1;
std::vector<int> societyIds;

// %EndTag(vars)%


// %Tag(Box)%
Marker makeBot( InteractiveMarker &msg )
{
  Marker marker;

  marker.type = Marker::CYLINDER;
  marker.scale.x = msg.scale * 0.33;
  marker.scale.y = msg.scale * 0.33;
  marker.scale.z = msg.scale * 0.15;
  marker.color.r = 0.1;
  marker.color.g = 0.1;
  marker.color.b = 1.0;
  marker.color.a = 1.0;

  return marker;
}

Marker makeCol( InteractiveMarker &msg )
{
  Marker marker;

  marker.type = Marker::CYLINDER;
  marker.scale.x = msg.scale * 0.1;
  marker.scale.y = msg.scale * 0.1;
  marker.scale.z = msg.scale * 0.9;
  marker.color.r = 0.1;
  marker.color.g = 0.1;
  marker.color.b = 1.0;
  marker.color.a = 1.0;

  return marker;
}

// %Tag(processFeedback)%
void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{

  server->applyChanges();
}

void makeMovingMarker( const tf::Vector3& position , int frame_id , int Robot_type)
{
  InteractiveMarker int_marker;
  std::stringstream ss;//create a stringstream
  ss << frame_id;//add number to the stream
  std::stringstream s_type;//create a stringstream
  s_type << Robot_type;//add number to the stream
  std::cout << "Robot_PF_" + ss.str() +"_"+ s_type.str() << std::endl;
  int_marker.header.frame_id =  "/Robot_PF_" + ss.str() +"_"+ s_type.str();
  tf::pointTFToMsg(position, int_marker.pose.position);
  int_marker.scale = 1;

  int_marker.name = "Robot_PF_" + ss.str() +"_"+ s_type.str();
  int_marker.description = "Robot_PF_" + ss.str() +"_"+ s_type.str();

  InteractiveMarkerControl control;

  control.orientation.w = 1;
  control.orientation.x = 1;
  control.orientation.y = 0;
  control.orientation.z = 0;
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);

  control.interaction_mode = InteractiveMarkerControl::MOVE_PLANE;
  control.always_visible = true;
  control.markers.push_back( makeBot(int_marker) );
  int_marker.controls.push_back(control);

  server->insert(int_marker);
  server->setCallback(int_marker.name, &processFeedback);

}

void makeColMarker( const tf::Vector3& position , int frame_id, int Robot_type )
{
  InteractiveMarker int_marker;
  std::stringstream ss;//create a stringstream
  ss << frame_id;//add number to the stream

  std::stringstream s_type;//create a stringstream
  s_type << Robot_type;//add number to the stream
  int_marker.header.frame_id =  "/Robot_PF_" + ss.str() +"_"+ s_type.str();
  std::cout << "Robot_PF_" + ss.str() +"_"+ s_type.str() << std::endl;
  tf::pointTFToMsg(position, int_marker.pose.position);
  int_marker.scale = 1;

  int_marker.name = "Robot_PF_" + ss.str() +"_"+ s_type.str();
  int_marker.description = "Robot_PF_" + ss.str() +"_"+ s_type.str();

  InteractiveMarkerControl control;

  control.orientation.w = 1;
  control.orientation.x = 1;
  control.orientation.y = 0;
  control.orientation.z = 0;
  control.interaction_mode = InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(control);

  control.interaction_mode = InteractiveMarkerControl::MOVE_PLANE;
  control.always_visible = true;
  control.markers.push_back( makeCol(int_marker) );
  int_marker.controls.push_back(control);

  server->insert(int_marker);
  server->setCallback(int_marker.name, &processFeedback);

}




// %Tag(frameCallback)%
void frameCallback(const droneMsgsROS::robotPose &msg)
{


  static tf::TransformBroadcaster br;

  tf::Transform t;

  ros::Time time = ros::Time::now();

  // msg->data has the drones' idDrone parameter
  bool not_found = true;
  for (std::vector<int>::const_iterator it = societyIds.begin();
       it != societyIds.end();
       ++it) {
      if ( (*it) == (msg.id_Robot) ) {
          not_found = false;
      }
  }
  // int value = atoi(msg.header.frame_id.c_str())

  if ( not_found ) { // msg->data not in societyIds

      societyIds.push_back(msg.id_Robot);

      // Create marker with new Id

      tf::Vector3 position;
      position = tf::Vector3( 0,0, 0);
      if (msg.id_Robot < 0){

        makeColMarker( position, counter, msg.Robot_Type);
        server->applyChanges();
        counter ++;
      }
      else if (msg.id_Robot > 0) {
        makeMovingMarker( position, counter_2, msg.Robot_Type);
        server->applyChanges();
        counter_2++;
      }


  }

  std::stringstream ss;//create a stringstream
  ss << abs(msg.id_Robot);//add number to the stream

  std::stringstream s_type;//create a stringstream
  s_type << msg.Robot_Type;//add number to the stream

  if (msg.id_Robot < 0){
    t.setOrigin(tf::Vector3(msg.x, msg.y, 0.45));
  }
  else {
    t.setOrigin(tf::Vector3(msg.x, msg.y, 0.0));
  }

  t.setRotation(tf::createQuaternionFromRPY(0.0, 0.0, -msg.theta + 3.1415));
  br.sendTransform(tf::StampedTransform(t, time, "base_link", "Robot_PF_" + ss.str() +"_"+ s_type.str()));


}

// %Tag(main)%
int main(int argc, char** argv)
{
  ros::init(argc, argv, "robotPF_controls");
  ros::NodeHandle n;

  // create a timer to update the published transforms
  // ros::Timer frame_timer = n.createTimer(ros::Duration(0.01), frameCallback);



  server.reset( new interactive_markers::InteractiveMarkerServer("robotPF_controls","",false) );

  //ros::Duration(0.1).sleep();

  ros::Subscriber sub = n.subscribe("rvizRobotPoses", 100, frameCallback);


  ros::spin();

  server.reset();

}
// %EndTag(main)%
